import os.path
import torch
import json
from datetime import date
from perin.config.params import Params
from perin.data.shared_dataset import SharedDataset
from perin.model.model import Model
from perin.data.batch import Batch

output_path = "perin_output.mrp"
checkpoint = {
	"amr": {"name": "./perin/pretrained_models/base_amr.h5", "path": "./perin/pretrained_models"},
	"ucca": {"name": "./perin/pretrained_models/base_ucca.h5", "path": "./perin/pretrained_models"}
}
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

def load_checkpoint(framework, device):
	filename, location = checkpoint[framework]["name"], checkpoint[framework]["path"]
	state_dict = torch.load(filename, map_location=device)
	args = Params().load_state_dict(state_dict["args"])
	
	dataset = SharedDataset(args)
	dataset.load_state_dict(args, state_dict["dataset"])
	
	model = Model(dataset, args, initialize=False).to(device).eval()
	model.load_state_dict(state_dict["model"])
	
	return model, dataset, args
	
def parse(input, model, dataset, args, framework, language, **kwargs):
	batches = dataset.load_sentences(input, args, framework, language)
	output = batches.dataset.datasets[dataset.framework_to_id[(framework, language)]].data
	output = list(output.values())
	
	for i, batch in enumerate(batches):
		with torch.no_grad():
			prediction = model(Batch.to(batch,device), inference=True, **kwargs)[(framework, language)][0]
		
		for key, value in prediction.items():
			output[i][key] = value
		
		output[i]["input"] = output[i]["sentence"]
		output[i] = {k: v for k, v in output[i].items() if k in {"id", "input", "nodes", "edges", "tops"}}
		output[i]["framework"] = framework
		output[i]["time"] = str(date.today())
		
	return output
	
def save(output, path):
	with open(path, "a", encoding="utf8") as f:
		for sentence in output:
			json.dump(sentence, f, ensure_ascii=False)
			f.write("\n")
			

def run_perin(sentences, framework="ucca"):
	model, dataset, args = load_checkpoint(framework, device)
	language = "eng"
	prediction = parse(sentences, model, dataset, args, framework, language)
	save(prediction, output_path)
	return prediction
