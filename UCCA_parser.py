import word_embeddings

input_sentence=""
nodes=[]
edges=[]


class Node:

	def __init__(self, id: int, anchors: (int, int), word: str, embedding: str):
		self.id = id
		self.anchors = anchors
		self.word = word
		self.embedding = embedding

	def set_id(self, ident):
		self.id = ident

	def set_embedding(self, embedding):
		self.embedding = embedding


def parse_nodes(nodes_dict):
	for node in nodes_dict:
		node_id = node['id']
		try:
			node_anchors = (node['anchors'][0]['from'], node['anchors'][0]['to'])
			word = input_sentence[node_anchors[0]:node_anchors[1]]
		except KeyError:
			node_anchors = (None, None)
			word = None
		if word:
			embedding = word_embeddings.get_word_embedding(word)
		else:
			embedding = None
		n = Node(id=node_id, anchors=node_anchors, word=word, embedding=embedding)
		nodes.append(n)


def find_node_by_id(id: int) -> Node:
	for node in nodes:
		if node.id == id:
			return node
	raise NameError("No node found with id = {id}".format(id=id))


class Edge:

	def __init__(self, source, target, label, attributes, values):
		self.source = source
		self.target = target
		self.label = label
		self.attributes = attributes
		self.values = values
		self.order = -1

	def set_order(self, order):
		self.order = order


def parse_edges(edges_json):
	for edge in edges_json:
		edge_source = find_node_by_id(edge['source'])
		edge_target = find_node_by_id(edge['target'])
		edge_label = edge['label']
		try:
			edge_attributes = edge['attributes']
		except KeyError:
			edge_attributes = None
		try:
			edge_values = edge['values']
		except KeyError:
			edge_values = None
		e = Edge(
			source=edge_source,
			target=edge_target,
			label=edge_label,
			attributes=edge_attributes,
			values=edge_values
		)
		edges.append(e)


def order_edges():
	for edge in edges:
		if edge.order != -1:
			continue
		other_edges = find_edges_with_same_source(edge.source)
		other_edges.sort(key=lambda x: x.target.id)
		for oe in other_edges:
			oe.set_order(order=other_edges.index(oe))

def find_edges_with_same_source(source):
	same_source_edges = []
	for edge in edges:
		if edge.source is source:
			same_source_edges.append(edge)
	return same_source_edges

def set_embedding_intermediate():
	empty_embeddings = True
	while empty_embeddings:
		make_embedding_list()
		empty_embeddings = False
		for node in nodes:
			if node.embedding is None:
				empty_embeddings = True
				break

def make_embedding_list():
	for node in nodes:
		embeddings = []
		if node.word is None and node.embedding is None:
			outgoing_edges = []
			for edge in edges:
				if edge.source == node and edge.label != "f":
					outgoing_edges.append(edge)
			outgoing_edges.sort(key=lambda x: x.order)
			for oe in outgoing_edges:
				if oe.target.embedding is None:
					embeddings = None
					break
				else:
					embeddings.append(oe.target.embedding)
			node.set_embedding(embeddings)

def parse_sentence(sentence_json):
	global input_sentence 
	input_sentence = sentence_json['input']
	global nodes
	nodes = []
	parse_nodes(sentence_json['nodes'])
	global edges
	edges = []
	parse_edges(sentence_json['edges'])
	order_edges()
	set_embedding_intermediate()





















