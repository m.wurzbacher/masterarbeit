import os, re, traceback
import coref_res, sent_split, perin_parsing, preprocessing, UCCA_parser, auraDB_connection, word_embeddings
import argparse

def read_docs(path):
	all_files = os.listdir(path)
	txt_files = filter(lambda x: x[-4:] == '.txt', all_files)
	docs = []
	for txt_file in txt_files:
		text = open(path+txt_file, 'r').read()
		docs.append(text)
	return docs

if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-e", "--extend", action='store_true')
	parser.add_argument("uri", help="The uri of your neo4j connection")
	parser.add_argument("user", help="The user of your neo4j connection")
	parser.add_argument("pw", help="The password of your neo4j connection")
	parser.set_defaults(extend=False)
	args = parser.parse_args()

	doc_location = "./texts/"
	docs = read_docs(doc_location)

	uri = args.uri
	user = args.user
	password = args.pw

	auraDB = auraDB_connection.AuraDatabase(uri, user, password)

	if not args.extend:
		auraDB.delete_db()
	
	word_embeddings.init_wembedding_model()

	for doc in docs:
		doc = preprocessing.prep(doc=doc)
		coref_res_doc=coref_res.replace_coref(doc=doc)
		sents=sent_split.sent_split_nltk(doc=coref_res_doc)
		for sent in sents:
			sent = re.sub(r"[^a-zA-z0-9 ]", "", sent)
			sent_graphs = []
			try:
				sent_graphs=perin_parsing.run_perin(sentences=[sent], framework="ucca")
			except Exception:
				print("PERIN Error for this sentence:")
				print(sent)
				traceback.print_exc()
			UCCA_parser.parse_sentence(sent_graphs[0])
			for node in UCCA_parser.nodes:
				auraDB.add_node(node)
			for edge in UCCA_parser.edges:
				if edge.label != "f":
					auraDB.create_semantic_relation(edge)
			auraDB.delete_orphaned_nodes()
	auraDB.close()		
