import spacy
import neuralcoref


def coref_init(doc):
	nlp = spacy.load('en_core_web_sm')
	neuralcoref.add_to_pipe(nlp)
	nlp_doc = nlp(doc)
	return nlp_doc


def replace_coref(doc):
	nlp_doc = coref_init(doc)

	if nlp_doc._.has_coref:
		return nlp_doc._.coref_resolved
	else:
		#print("No corefs")
		return doc


def get_coref_clusters(doc):
	nlp_doc = coref_init(doc)
	
	if nlp_doc._.has_coref:
		return nlp_doc._.coref_clusters
	else:
		#print("No corefs")
		return None
	
def write_corefs_to_file(doc):
	f = open("coref_test.txt", "a")
	f.write(doc)
	f.write("\n\n")
	f.write(str(get_coref_clusters(doc=doc)))
	f.write("\n\n")
	f.write(str(replace_coref(doc=doc)))
	f.write("\n\n")
	f.write("________________________________\n")
	f.close()














