## How to install

- Clone the repository using `git clone`
- Use `pip3 requirements.txt` to install all needed modules
- Download the semantic parsing model PERIN from `https://github.com/ufal/perin/` as stated there.
- Then go to the PERIN folder (`cd perin`) and create a directory for the pretrained models (`mkdir pretrained_models`). Then download the `base_ucca.h5` pretrained model from the link on the PERIN github page (see bullet point above) and place it into the `pretrained_models` folder. Then go back to the parent directory.
- Download the required `spacy` model using `python -m spacy download en_core_web_sm`
- Download the required `nltk` tokenizer: Open the python shell, execute `import nltk.data` and `nltk.download('punkt')`, then close the python shell again

## How to use

- Copy the texts you want to parse in the `./texts` folder
- Run the `main.py` file as follows: `main.py uri user pw` where `uri` must be replaced by the uri of your neo4j database connection, `user` must be replaced by the user of your neo4j database connection and `pw` must be replaced by the password of your neo4j database connection
- If you want to add onto an already existing database you can use `-e` or `--extend` as a flag on the command line when running the `main.py` file. If you don't use this flag, the nodes and edges of the existing database will be deleted and a new ones will be set up.
- The `run_main.sh` script can be used to run the pipeline with slurm. It has been tested to work on the GWDG HPC cluster and only the neo4j credentials need to be adapted.

