from neo4j import GraphDatabase
from neo4j.exceptions import ServiceUnavailable
import UCCA_parser

class AuraDatabase:

	def __init__(self, uri, user, password):
		self.driver = GraphDatabase.driver(uri, auth=(user, password))

	def close(self):
		self.driver.close()

	def add_node(self, node):
		if (self.find_word_by_name(node_word=node.word) is None):
			self.create_node(node=node)
		else:
			old_node = node
			id_new = self.find_word_by_name(node_word=node.word)
			new_node = old_node
			new_node.set_id(ident=id_new)
			self.adjust_edges(old_node=old_node, new_node=new_node)

	def adjust_edges(self, old_node, new_node):
		for edge in UCCA_parser.edges:
			if (edge.source == old_node):
				edge.source = new_node
			if (edge.target == old_node):
				edge.target = new_node

	def create_node(self, node):
		with self.driver.session() as session:
			if (node.word is None):
				result = session.write_transaction(
					self._create_and_return_intermediate, node=node
				)
			else:
				result = session.write_transaction(
					self._create_and_return_word, node=node
				)
			node.set_id(result.values()[0].id)
			#print(result)

	@staticmethod
	def _create_and_return_word(tx, node):
		query = (
			"CREATE (w1:Word { name: $nodeword, embedding: $nodeembedding}) "
			"RETURN w1"
		)
		result = tx.run(query, nodeword=node.word, nodeembedding=str(node.embedding))
		try:
			return result.single()
		except ServiceUnavailable as exception:
			print("Service-Unavailable error when creating word node for word " + node.word)
			raise

	@staticmethod
	def _create_and_return_intermediate(tx, node):
		query = (
			"CREATE (i1:Intermediate {name: $nodelabel, embedding: $nodeembedding}) "
			"RETURN i1"
		)
		result = tx.run(query, nodelabel=node.id, nodeembedding=str(node.embedding))
		try:
			return result.single()
		except ServiceUnavailable as exception:
			print("Service-Unavailable error when creating intermediate node for " + node.id)
			raise

	def find_word_by_name(self, node_word):
		with self.driver.session() as session:
			result = session.read_transaction(self._find_and_return_word_by_name, node_word=node_word)
			if result is None:
				return None
			else:
				return result.get("wordid")

	@staticmethod
	def _find_and_return_word_by_name(tx, node_word):
		query = (
			"MATCH (w:Word) "
			"WHERE w.name = $node_word "
			"RETURN ID(w) AS wordid"
		)
		result = tx.run(query, node_word=node_word)
		return result.single()

	def create_semantic_relation(self, edge):
		if self.find_relationship(edge=edge):
			return
		with self.driver.session() as session:
			result = session.write_transaction(
				self._create_and_return_semantic_relation, edge=edge
			)
			#print(result)

	@staticmethod
	def _create_and_return_semantic_relation(tx, edge):
		query = AuraDatabase.build_query_for_relationship(edge=edge)
		result = tx.run(query, edge_source=edge.source.id, edge_target=edge.target.id, edge_order=edge.order)
		try:
			return result.values()
		except ServiceUnavailable as exception:
			print("Service-Unavailable error when creating semantic relationship for edge from " + str(edge.source.id) + " to " + str(edge.target.id))
			raise

	@staticmethod
	def get_abbrev_to_category(abbrev):
		abbrev_to_category = {
			"p": "PROCESS",
			"s": "STATE",
			"a": "PARTICIPANT",
			"d": "ADVERBIAL",
			"c": "CENTER",
			"e": "ELABORATOR",
			"n": "CONNECTOR",
			"r": "RELATOR",
			"h": "PARALLEL_SCENE",
			"l": "LINKER",
			"g": "GROUND",
			"f": "FUNCTION",
			"u": "PUNCTUATION",
			"t": "TIME"
		}
		return abbrev_to_category[abbrev]

	@staticmethod
	def build_query_for_relationship(edge):
		edge_label = AuraDatabase.get_abbrev_to_category(edge.label)
		if (edge.source.word is None and edge.target.word is None):
			query = (
				"MATCH (a:Intermediate), (b:Intermediate) "
				"WHERE ID(a) = $edge_source AND ID(b) = $edge_target "
				"CREATE (a)-[r:"+edge_label+"{order: $edge_order}]->(b) "
				"RETURN a, b"
			)
		elif (edge.source.word is None and edge.target.word):
			query = (
				"MATCH (a:Intermediate), (b:Word) "
				"WHERE ID(a) = $edge_source AND ID(b) = $edge_target "
				"CREATE (a) -[r:"+edge_label+"{order: $edge_order}]->(b) "
				"RETURN a, b"
			)
		elif (edge.source.word and edge.target.word is None):
			query = (
				"MATCH (a:Word), (b:Intermediate) "
				"WHERE ID(a) = $edge_source AND ID(b) = $edge_target "
				"CREATE (a)-[r:"+edge_label+"{order: $edge_order}]->(b) "
				"RETURN a, b"
			)
		else:
			query = (
				"MATCH (a:Word), (b:Word) "
				"WHERE ID(a) = $edge_source AND ID(b) = $edge_target "
				"CREATE (a)-[r:"+edge_label+"{order: $edge_order}]->(b) "
				"RETURN a, b"
			)
		return query

	def find_relationship(self, edge):
		with self.driver.session() as session:
			result = session.read_transaction(
				self._find_relationship, edge=edge
			)
		return result

	@staticmethod
	def _find_relationship(tx, edge):
		edge_label = AuraDatabase.get_abbrev_to_category(edge.label)
		query = (
			"MATCH (a)-[r:"+edge_label+"]->(b) "
			"WHERE ID(a) = $edge_source AND ID(b) = $edge_target "
			"RETURN r"
		)
		result = tx.run(query, edge_source=edge.source.id, edge_target=edge.target.id)
		return result.values()

	def delete_db(self):
		with self.driver.session() as session:
			session.write_transaction(
				self._delete_db
			)

	@staticmethod
	def _delete_db(tx):
		query = (
			"MATCH (n) DETACH DELETE n"
		)
		tx.run(query)

	def delete_orphaned_nodes(self):
		with self.driver.session() as session:
			session.write_transaction(
				self._delete_orphaned_nodes
			)

	@staticmethod
	def _delete_orphaned_nodes(tx):
		query = (
			"MATCH (n:Word) "
			"WHERE NOT (n)--() "
			"DELETE n"
		)
		tx.run(query)

