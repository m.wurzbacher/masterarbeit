#!/bin/bash
#SBATCH -p gpu
#SBATCH -t 10:00
#SBATCH -n 4
#SBATCH -G rtx5000:1

module load cuda/11.1.0

python main.py uri user password
