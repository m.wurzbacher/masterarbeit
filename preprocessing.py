import re
import contractions

def prep(doc):
	#everything to lower case
	doc = doc.lower()

	#expand abbreviations and contractions
	doc = expand_abbreviations(doc=doc)
	doc = contractions.fix(doc)

	#remove problematic whitespaces and special characters
	doc = doc.replace("\n", " ")
	doc = doc.replace("\n", " ")
	doc = re.sub(r"\s{2,}", " ", doc)

	#remove genitive 's clitic
	doc = re.sub(r"'s", "", doc)
	
	return doc

def expand_abbreviations(doc):
	doc = re.sub(r"approx\.", "approximately", doc)
	doc = re.sub(r"appt\.", "appointment", doc)
	doc = re.sub(r"apt\.", "apartment", doc)
	doc = re.sub(r"dept\.", "department", doc)
	doc = re.sub(r"est\.", "established", doc)
	doc = re.sub(r"min\.", "minimum", doc)
	doc = re.sub(r"misc\.", "miscellaneous", doc)
	doc = re.sub(r"mr\.", "mister", doc)
	doc = re.sub(r"mrs\.", "missus", doc)
	doc = re.sub(r"ms\.", "miss", doc)
	doc = re.sub(r"no\.", "number", doc)
	doc = re.sub(r"vs\.", "versus", doc)
	doc = re.sub(r"corp\.", "corporation", doc)
	doc = re.sub(r"inc\.", "incorporated", doc)
	doc = re.sub(r"e\.g\.", "for example", doc)
	doc = re.sub(r"etc\.", "and so on", doc)
	doc = re.sub(r"i\.e\.", "that is", doc)
	doc = re.sub(r"et al\.", "and others", doc)
	doc = re.sub(r"q\.e\.d\.", "which was to be demonstrated", doc)
	doc = re.sub(r"jr\.", "junior", doc)
	doc = re.sub(r"snr\.", "senior", doc)
	doc = re.sub(r"u\.s\.a", "usa", doc)
	doc = re.sub(r"dr\.", "doctor", doc)
	return doc
