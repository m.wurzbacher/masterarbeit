import gensim.downloader as api

def init_wembedding_model():
	global model
	model = api.load("fasttext-wiki-news-subwords-300")

def get_word_embedding(word: str):
	return model[word]
