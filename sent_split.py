import nltk.data
nltk.download('punkt')
import re

def sent_split_nltk(doc):
	tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
	return tokenizer.tokenize(doc)
	
def write_sents_to_file(doc):
	f = open("sent_split.txt","a")
	f.write(doc)
	f.write("\n\n")
	sents = sent_split_nltk(doc=str(doc))
	for sent in sents:
		f.write(sent)
		f.write("\n\n")
	f.write("-------------------------------\n\n")
